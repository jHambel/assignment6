package src;

//Efficiency equation = (x + day)%7= newDay
public class Day {
  
    String day;
    String[] week= new String[7];
  
    public Day(){
      
        fillWeek();
        day= week[0];
    }
  
    public Day(String d){
      
        fillWeek();
        day= week[searchWeek(d)];
    }
  
    public void setDay(String d){
      
        day=week[searchWeek(d)];
    }
  
    public String getDay(){
      
        return day;
    }
  
    public void printDay(){
      
        System.out.println("Today: " + day);

    }
  
    public void fillWeek(){
      
        week[0]= "Sunday";
        week[1]= "Monday";
        week[2]= "Tuesday";
        week[3]= "Wednesday";
        week[4]= "Thursday";
        week[5]= "Friday";
        week[6]= "Saturday";
    }
  
    public int searchWeek(String d){
      
        for(int i=0;i<7;i++){
          
            if(week[i].equals(d)){
              
                return i;
            }
        }
      
        if(day.equals(null)){
            System.out.println("Thats not a day Silly! Please enter a real day: ");
        }
        return 8;
    }
  
    public String getNextDay(){
      
        int temp= searchWeek(day);
      
        if(temp== 6){
          
            return week[0];
        }else{
          
            return week[temp+1];
        }
    }
  
    public String getPreviousDay(){
      
        int temp= searchWeek(day);
      
        if(temp== 0){
          
            return week[6];
        }else{
          
            return week[temp-1];
        }
    }
  
    public String addToDay(int x){
      
        int temp= searchWeek(day);
      
        for(int i=0;i<x;i++){
          
            if(temp==6){
              
                temp=0;
            }else{
              
                temp++;
            }
        }
      
        return week[temp];
    }
  
    public String subtractFromDay(int x){
      
        int temp= searchWeek(day);
      
        for(int i=0;i<x;i++){
          
            if(temp==0){
              
                temp=6;
            }else{
              
                temp--;
            }
        }
      
        return week[temp];
    }
  
}