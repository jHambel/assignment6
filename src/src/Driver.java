package src;

import java.util.Scanner;

import src.Day;

public class Driver {
   
    static Day day= new Day();
    static Scanner scan= new Scanner(System.in);
    static String temp;
   
    public static void checkYorN(){
       
        System.out.println("Would you like to change the date? y or n: ");
        temp= scan.next();
       
        if(temp.equals("y")){
           
            checkSetting();
            outputData();
            checkAddOrNah();
        }else if(temp.equals("n")){
           
            checkAddOrNah();
        }else{
           
            System.out.println("That's not an acceptable answer! ");
            checkYorN();
        }
    }
   
    public static void checkSetting(){
       
        System.out.println("What day would you like to change to: ");
        temp= scan.next();
       
        if(day.searchWeek(temp) != 8){
           
            day.setDay(temp);
        }else{
           
            System.out.println("Thats not a day silly! Please enter a day!");
            checkSetting();
        }
    }
   
    public static void checkAddOrNah(){
       
        System.out.println("Would you like to add or subtract the date... or nah: ");
        temp= scan.next();
       
        if(temp.equals("add")){
        	
           System.out.println("How many days do you want to add? "); 
           day.setDay(day.addToDay(scan.nextInt()));
           
           outputData(); 
        }else if(temp.equals("subtract")){
           
        	System.out.println("How many days do you want to subtract? "); 
            day.setDay(day.subtractFromDay(scan.nextInt()));
            
            outputData();
        }else if(temp.equals("nah")){
           
        	System.out.println("WELL MAYBE I DONT LIKE YOU EITHER! "); 
            outputData();
        }else{
           
            System.out.println("That choice is UNACCEPTABLE!!!!!!! ");
            checkAddOrNah();
        }
    }
    public static void outputData(){

        day.printDay();
        System.out.println("Tommorow: " + day.getNextDay());
        System.out.println("Yesterday: "+ day.getPreviousDay());
    }
   
    public static void main(String[] args){
       
        checkYorN();       
    }
}